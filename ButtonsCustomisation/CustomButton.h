//
//  CustomButton.h
//  ButtonsCustomisation
//
//  Created by User on 11/13/15.
//  Copyright © 2015 User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomButton : UIControl
@property (nonatomic,strong) UIImage* backgroundImage;
@property (nonatomic,strong) UILabel* textLabel;

-(instancetype) initWithImage: (UIImage*) image;
-(instancetype) initWithRoundedRadiusFrame: (CGRect) frame;
-(void) makeButtonRounded: (float) cornerRadius;
@end
