//
//  CustomButton.m
//  ButtonsCustomisation
//
//  Created by User on 11/13/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "CustomButton.h"

@implementation CustomButton
{
    UIImageView* imageView;
    UIView* highlitedView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //properties
      //  _backgroundImage = [[UIImage alloc] init];
        _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.userInteractionEnabled = NO;
        //variables
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        imageView.userInteractionEnabled = NO;
        
        highlitedView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        highlitedView.userInteractionEnabled = NO;
        highlitedView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.25];
        //add subview
        
        [self addSubview:imageView];
        [self addSubview:_textLabel];
   
        //positions
        


    }
    return self;
}
-(instancetype) initWithRoundedRadiusFrame: (CGRect) frame;
{
    self = [self initWithFrame:frame];
    if (self) {
        self.clipsToBounds= YES;
        imageView.clipsToBounds = YES;
       self.layer.cornerRadius = frame.size.width/2.0f;
       
    }
    return self;

    //    button.clipsToBounds = YES;
    //
    //    //half of the width
    //    button.layer.cornerRadius = ROUND_BUTTON_WIDTH_HEIGHT/2.0f;
    //    button.layer.borderColor=[UIColor redColor].CGColor;
    //    button.layer.borderWidth=2.0f;
}
- (void)layoutSubviews
{
    [super layoutSubviews];

    
}
-(void) makeButtonRounded: (float) cornerRadius
{
    self.clipsToBounds = YES;
    float minSide = 0;
    if (self.frame.size.height<self.frame.size.width) {
        minSide = self.frame.size.height;
    }
    else
    {
        minSide = self.frame.size.width;
    }
    self.layer.cornerRadius = self.frame.size.height/2.0f;
}
- (void)setBackgroundImage:(UIImage *)backgroundImage
{

    imageView.image = backgroundImage;
    
}
-(instancetype) initWithImage: (UIImage*) image
{
  CGRect frame= CGRectMake(0, 0, image.size.width, image.size.height);
    self = [self initWithFrame:frame];
    if (self) {
        self.backgroundImage = image;
        
    }
    return self;
}

-(void)dealloc
{
    
  
}
-(void)removeFromSuperview
{
   
    [super removeFromSuperview];
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    
    [super touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
    [super touchesMoved:touches withEvent:event];
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    
}
- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    [self addSubview:highlitedView];
    return YES;
}

- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
    return YES;
}
- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    [highlitedView removeFromSuperview];
}
@end
