//
//  ViewController.m
//  ButtonsCustomisation
//
//  Created by User on 11/13/15.
//  Copyright © 2015 User. All rights reserved.
//

#import "ViewController.h"
#import "CustomButton.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
   
    CustomButton* button = [[CustomButton alloc] initWithFrame: CGRectMake(100, 100, 100, 100)];
    button.backgroundColor = [UIColor redColor];
    button.textLabel.text = @"Button";
    button.textLabel.textColor = [UIColor whiteColor];
    [button addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
   button.backgroundImage = [UIImage imageNamed:@"test_image"];
    [button makeButtonRounded:100];
    [self.view addSubview:button];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) click
{
    NSLog(@"clicked");
}
@end
